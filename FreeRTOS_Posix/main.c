#include <stdio.h>
#include <stdlib.h>
// #include "main.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "stream_buffer.h"
#include "message_buffer.h"
#include "event_groups.h"

static void vTask1(void *pvParameters);
static void vTask2(void *pvParameters);

StreamBufferHandle_t xUserStream = NULL;
MessageBufferHandle_t xUserMessage = NULL;

const size_t xStreamBufferSizeBytes = 100, xTriggerLevel = 10;

void vTimerCallback(TimerHandle_t xTimer)
{
    uint32_t ulCount;
    static num = 0;

    ulCount = (uint32_t)pvTimerGetTimerID(xTimer);
    num++;

    printf("vTimer id is %d:%d\r\n", ulCount, num);
}

int main()
{
    // Task and Queue
    static xQueueHandle xTestQueue;
    xTestQueue = xQueueCreate(20, (unsigned portBASE_TYPE)sizeof(unsigned short));
    xTaskCreate(vTask1, "vTask1", configMINIMAL_STACK_SIZE, (void *)&xTestQueue, tskIDLE_PRIORITY, NULL);
    xTaskCreate(vTask2, "vTask2", configMINIMAL_STACK_SIZE, (void *)&xTestQueue, tskIDLE_PRIORITY, NULL);

    // stream buffer
    xUserStream = xStreamBufferCreate(xStreamBufferSizeBytes, xTriggerLevel);
    if (xUserStream == NULL)
    {
        ; // error
    }
    else
    {
        if (xStreamBufferIsEmpty(xUserStream) == pdTRUE)
        {
            printf("xUserStream is empty\r\n");
        }
    }

    // message buffer
    xUserMessage = xMessageBufferCreate(xStreamBufferSizeBytes);
    if (xUserMessage == NULL)
    {
        ; // error
    }
    else
    {
        if (xMessageBufferIsEmpty(xUserMessage) == pdTRUE)
        {
            printf("xUserMessage is empty\r\n");
        }
    }

    vTaskStartScheduler();
    return 1;
}

static void vTask1(void *pvParameters)
{
    unsigned short usValue = 0, usLoop;
    xQueueHandle *pxQueue;
    const unsigned short usNumToProduce = 3;
    short sError = pdFALSE;

    pxQueue = (xQueueHandle *)pvParameters;

    EventBits_t uxBits = 0;

    size_t xBytesSent;

    for (;;)
    {
        for (usLoop = 0; usLoop < usNumToProduce; ++usLoop)
        {
            /* Send an incrementing number on the queue without blocking. */
            printf("Task1 will send: %d\r\n", usValue);
            if (xQueueSendToBack(*pxQueue, (void *)&usValue, (portTickType)0) != pdPASS)
            {
                sError = pdTRUE;
            }
            else
            {
                ++usValue;
                printf("queue remain: %d\r\n", uxQueueSpacesAvailable(*pxQueue));
            }
        }
        xBytesSent = xStreamBufferSend(xUserStream, "helloworld", 1, pdMS_TO_TICKS(100));
        if (xBytesSent != 1)
        {
            printf("xStreamBufferSend no enough space %d.\r\n", xBytesSent);
        }
        else{
            printf("xStreamBufferSend space %d.\r\n", xStreamBufferBytesAvailable(xUserStream));
        }
        xBytesSent = xMessageBufferSend(xUserMessage, "helloworld", 5, pdMS_TO_TICKS(100));
        if (xBytesSent != 5)
        {
            printf("xMessageBufferSend no enough space %d.\r\n", xBytesSent);
        }
        else{
            printf("xMessageBufferSend space %d.\r\n",  xMessageBufferSpacesAvailable(xUserMessage));
        }
        vTaskDelay(1000);
    }
}

static void vTask2(void *pvParameters)
{
    unsigned short usData = 0;
    xQueueHandle *pxQueue;

    pxQueue = (xQueueHandle *)pvParameters;

    uint8_t ucRxData[20];
    size_t xRxDataByte = 0;
    const TickType_t xBlockTime = pdMS_TO_TICKS(20);

    for (;;)
    {
        while (uxQueueMessagesWaiting(*pxQueue))
        {
            if (xQueueReceive(*pxQueue, &usData, (portTickType)0) == pdPASS)
            {
                printf("Task2 received:%d\r\n", usData);
            }
        }
        memset(ucRxData, 0, sizeof(ucRxData));
        xRxDataByte = xStreamBufferReceive(xUserStream, ucRxData, sizeof(ucRxData), xBlockTime);
        if (xRxDataByte > 0)
        {
            printf("xStreamBufferReceive [%d]%s\r\n", xRxDataByte, ucRxData);
        }
        else{
            printf("xStreamBufferReceive no receive\r\n");
        }
        memset(ucRxData, 0, sizeof(ucRxData));
        xRxDataByte = xMessageBufferReceive(xUserMessage, ucRxData, sizeof(ucRxData), xBlockTime);
        if (xRxDataByte > 0)
        {
            printf("xMessageBufferReceive [%d]%s\r\n", xRxDataByte, ucRxData);
        }
        else{
            printf("xMessageBufferReceive no receive\r\n");
        }
        vTaskDelay(5000);
    }
}

/********************************************************/
/* This is a stub function for FreeRTOS_Kernel */
void vMainQueueSendPassed(void)
{
    return;
}

/* This is a stub function for FreeRTOS_Kernel */
void vApplicationIdleHook(void)
{
    return;
}
