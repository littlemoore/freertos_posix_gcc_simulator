#include <stdio.h>
#include <stdlib.h>
// #include "main.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "stream_buffer.h"
#include "message_buffer.h"
#include "event_groups.h"

static void vTask1(void *pvParameters);
static void vTask2(void *pvParameters);

TimerHandle_t xUserTimer = NULL;

#define BIT_0 (1 << 0)
#define BIT_1 (1 << 1)
#define BIT_2 (1 << 2)
#define BIT_3 (1 << 3)
#define BIT_4 (1 << 4)

EventGroupHandle_t xUserEvent = NULL;

void vTimerCallback(TimerHandle_t xTimer)
{
    uint32_t ulCount;
    static num = 0;

    ulCount = (uint32_t)pvTimerGetTimerID(xTimer);
    num++;

    printf("vTimer id is %d:%d\r\n", ulCount, num);
}

int main()
{
    // Task and Queue
    static xQueueHandle xTestQueue;
    xTestQueue = xQueueCreate(20, (unsigned portBASE_TYPE)sizeof(unsigned short));
    xTaskCreate(vTask1, "vTask1", configMINIMAL_STACK_SIZE, (void *)&xTestQueue, tskIDLE_PRIORITY, NULL);
    xTaskCreate(vTask2, "vTask2", configMINIMAL_STACK_SIZE, (void *)&xTestQueue, tskIDLE_PRIORITY, NULL);

    // Timer
    xUserTimer = xTimerCreate("Timer", 1000 / portTICK_PERIOD_MS, pdTRUE, (void *)0, vTimerCallback);
    if (xUserTimer == NULL)
    {
        ;
    }
    else
    {
        if (xTimerStart(xUserTimer, 0) != pdPASS)
        {
            ;
        }
    }

    // EventGroup

    xUserEvent = xEventGroupCreate();

    if (xUserEvent == NULL)
    {
        ; // error
    }

    vTaskStartScheduler();
    return 1;
}

static void vTask1(void *pvParameters)
{
    unsigned short usValue = 0, usLoop;
    xQueueHandle *pxQueue;
    const unsigned short usNumToProduce = 3;
    short sError = pdFALSE;

    pxQueue = (xQueueHandle *)pvParameters;

    EventBits_t uxBits = 0;

    for (;;)
    {
        for (usLoop = 0; usLoop < usNumToProduce; ++usLoop)
        {
            /* Send an incrementing number on the queue without blocking. */
            printf("Task1 will send: %d\r\n", usValue);
            if (xQueueSendToBack(*pxQueue, (void *)&usValue, (portTickType)0) != pdPASS)
            {
                sError = pdTRUE;
            }
            else
            {
                ++usValue;
                printf("queue remain: %d\r\n", uxQueueSpacesAvailable(*pxQueue));
            }
        }
        uxBits = xEventGroupGetBits(xUserEvent);
        printf("user event [%04X]\r\n", uxBits);
        if ((uxBits & (BIT_0 | BIT_1)) == (BIT_0 | BIT_1))
        {
            xEventGroupClearBits(xUserEvent,(BIT_0));
        }

        const TickType_t xTicksToWait = 100 / portTICK_PERIOD_MS;

		uxBits = xEventGroupWaitBits(
					xUserEvent,
					BIT_0 | BIT_1,	// The bits within the event group to wait for.
					pdTRUE,			// BIT_0 and BIT_4 should be cleared before returning.
					pdFALSE,		// Don't wait for both bits, either bit will do.
					xTicksToWait );	// Wait a maximum of 100ms for either bit to be set.

		if( ( uxBits & ( BIT_0 | BIT_1 ) ) == ( BIT_0 | BIT_1 ) )
		{
			printf("xEventGroupWaitBits() returned because both bits were set.\r\n");
		}
		else if( ( uxBits & BIT_0 ) != 0 )
		{
			printf("xEventGroupWaitBits() returned because just BIT_0 was set.\r\n");
		}
		else if( ( uxBits & BIT_1 ) != 0 )
		{
			printf("xEventGroupWaitBits() returned because just BIT_1 was set.\r\n");
		}
		else
		{
			printf("xEventGroupWaitBits() returned because xTicksToWait ticks passed.\r\n");
			// without either BIT_0 or BIT_4 becoming set.
		}

        uxBits = xEventGroupSync(xUserEvent,BIT_4,(BIT_3|BIT_4),xTicksToWait);
        printf("user Sync event [%04X]\r\n", uxBits);
        if( ( uxBits & ( BIT_3 ) ) )
		{
			printf("xEventGroupSync() returned because both bits were set.\r\n");
		}
        else
		{
			printf("xEventGroupSync() returned because xTicksToWait ticks passed.\r\n");
			// without either BIT_0 or BIT_4 becoming set.
		}

        vTaskDelay(1000);
    }
}

static void vTask2(void *pvParameters)
{
    unsigned short usData = 0;
    xQueueHandle *pxQueue;

    pxQueue = (xQueueHandle *)pvParameters;

    EventBits_t uxBits = 0;

    for (;;)
    {
        while (uxQueueMessagesWaiting(*pxQueue))
        {
            if (xQueueReceive(*pxQueue, &usData, (portTickType)0) == pdPASS)
            {
                printf("Task2 received:%d\r\n", usData);
            }
        }
        uxBits = xEventGroupSetBits(xUserEvent, BIT_0 | BIT_1);
        if ((uxBits & (BIT_0 | BIT_1)) == (BIT_0 | BIT_1))
        {
            printf("user set event [%04X]\r\n", uxBits);
        }
        else if ((uxBits & BIT_1) != 0)
        {
            printf("user only set BIT_1\r\n");
        }
        else if ((uxBits & BIT_0) != 0)
        {
            printf("user only set BIT_0\r\n");
        }
        uxBits = xEventGroupSync(xUserEvent,BIT_3,(BIT_3|BIT_4),portMAX_DELAY);
        printf("user Sync2 event [%04X]\r\n", uxBits);
        printf("UNLOCK\r\n");
        vTaskDelay(5000);
    }
}

/********************************************************/
/* This is a stub function for FreeRTOS_Kernel */
void vMainQueueSendPassed(void)
{
    return;
}

/* This is a stub function for FreeRTOS_Kernel */
void vApplicationIdleHook(void)
{
    return;
}
